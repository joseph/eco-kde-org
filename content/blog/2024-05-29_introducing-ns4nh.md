---
date: 2024-05-29
title: "Opt Green: KDE Eco's New Sustainable Software Project"
categories: [KDE Eco, Sustainable Software, Sustainable Hardware]
author: Joseph P. De Veaugh-Geiss
summary: 'KDE Eco has begun a new initiative "Opt Green: Sustainable Software For Sustainable Hardware".'
featured: true
thumbnail : /blog/thumbnails/blog-opt-green-optimized.png
SPDX-License-Identifier: CC-BY-SA-4.0
authors:
- SPDX-FileCopyrightText: 2024 Joseph P. De Veaugh-Geiss <joseph@kde.org>
---

Inspired by the successes of the ["Blauer Engel Für FOSS" (BE4FOSS)](https://eco.kde.org/blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification/) project and KDE's ongoing [Sustainable Software](https://community.kde.org/Goals/Sustainable_Software) goal, KDE Eco has begun a new initiative: "[Opt Green](https://invent.kde.org/teams/eco/opt-green): Sustainable Software For Sustainable Hardware" (German: [*Nachhaltige Software Für Nachhaltige Hardware*](https://www.umweltbundesamt.de/das-uba/was-wir-tun/foerdern-beraten/verbaendefoerderung/projektfoerderungen-projekttraeger/opt-green-nachhaltige-software-fuer-nachhaltige)).

## Opt Green: Sustainable Software For Sustainable Hardware

By design, Free Software guarantees [transparency and user autonomy](https://fsfe.org/freesoftware/index.en.html). This gives *you*, the user, control of *your* hardware by removing unnecessary vendor dependencies. With Free Software, you're able to use your devices how you want, for as long as you want. There's no [bloatware](https://en.wikipedia.org/wiki/Software_bloat) and you can block [unwanted data use and ads](https://groenlinks.nl/sites/groenlinks/files/2021-09/CE_Delft_210166_Carbon_footprint_unwanted_data-use_smartphones.pdf) from driving up energy demands and slowing down your device&mdash;while shutting the door to uninvited snooping in your private life as well. With software made for your needs and not the vendors', you can choose applications designed for the hardware you already own. Say goodbye to premature hardware obsolescence: lean, efficient Free Software runs on devices which are decades old!

Independent and sustainable Free Software is good for the users, and good for the environment.

{{< container class="text-center" >}}

![The ["Think Global, Act Local" campaign](https://en.wikipedia.org/wiki/Think_globally,_act_locally) urged people to consider global health while taking action in their local communities. This new project urges people to do the same, but with computing. (Image from Karanjot Singh published under a [CC-BY-4.0](https://spdx.org/licenses/CC-BY-4.0.html) license.)](/blog/images/2024-05-24_ThinkGlobalComputeLocal.png)

{{< /container >}}

Over the next two years, the "Opt Green" initiative will bring what KDE Eco has been doing for sustainable software directly to end users. A particular target group for the project is those whose consumer behavior is driven by principles related to the environment, and not just price or convenience: the "eco-consumers".

Through online and offline campaigns as well as installation workshops, we will demonstrate the power of Free Software to drive down resource and energy consumption, and keep devices in use for the lifespan of the hardware, not the software.

Our motto: **The most environmentally-friendly device is the one you already own.**

The topic of software-driven sustainability is relevant for all Free Software applications and developers. We'd love to have you join us and become partners in combatting the issue of software-driven environmental harm. Check out the project's [Invent repository](https://invent.kde.org/teams/eco/opt-green) or the [contact page](https://eco.kde.org/get-involved/) to get involved today!

## Software's Environmental Harm

On [14 October 2025](https://learn.microsoft.com/en-us/lifecycle/faq/windows), the end of support for Windows 10 is [estimated](https://www.canalys.com/insights/end-of-windows-10-support-could-turn-240-million-pcs-into-e-waste) to make e-waste out of [240 million computers](https://www.reuters.com/technology/microsoft-ending-support-windows-10-could-send-240-mln-pcs-landfills-report-2023-12-21/) ineligible for the upgrade to Windows 11. Moreover, macOS support for Intel-based Apple computers&mdash;the last of which were sold in 2020&mdash;is [predicted](https://arstechnica.com/gadgets/2023/07/with-macos-sonoma-intel-macs-are-still-getting-fewer-updates-than-they-used-to/) to end (at the earliest) one year later in 2026, rendering even more millions upon millions of functioning devices obsolete. When users have no control over the software they rely on, they are left at a security risk when software support ends ... unless, of course, they purchase a new computer. (By comparison, consider that only in 2022 did Linus Torvalds first [suggest ending Linux kernel support](https://www.phoronix.com/news/Intel-i486-Linux-Possible-Drop) for Intel 486 processors from 1989. That's 33 years of support!)

Vendors frequently require buying a new device to support software updates. All too often, this is driven by economic imperatives rather than technological requirements. Moreover, while new hardware has become more and more powerful, new software offering [similar or identical functionality](https://www.umweltbundesamt.de/publikationen/entwicklung-anwendung-von-bewertungsgrundlagen-fuer) has frequently become less efficient and more energy-intensive, which has rendered older, less powerful devices useless.

Already in 2015 Achim Steiner, former Executive Director of the UN Environment Programme (UNEP), [warned](https://news.un.org/en/story/2015/05/497772) of the "tsunami of electronic waste rolling out over the world".

In 2016, 44.7 million tonnes of e-waste were generated, estimated to be equivalent to 4500 Eiffel Towers. If you were to stack those Eiffel Towers on top of each other, the result would be 17 times higher than Mount Everest.

By 2017, [United Nations University](https://web.archive.org/web/20220901055133/https://unu.edu/media-relations/releases/ewaste-rises-8-percent-by-weight-in-2-years.html) determined e-waste to be the [fastest growing waste stream](https://www.weforum.org/reports/a-new-circular-vision-for-electronics-time-for-a-global-reboot/) in the world.

In 2022, the amount of e-waste reached 59.4 million tonnes, a 33% increase since 2016.

The flow of e-waste continues to rise today.

{{< container class="text-center" >}}

![In 2016, 44.7 million metric tons of e-waste was generated. This is estimated to be equivalent to 4,500 Eiffel Towers, which, when stacked, is 17 times higher than Mount Everest. (Image from KDE published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license. Design by Anita Sengupta.)](/blog/images/mountain.png)

{{< /container >}}

Software is a frequently unacknowledged yet significant factor for sustainability. Software determines a hardware's energy consumption and minimum system requirements. It determines how long a device can remain safely in use. With software running on [everyday devices](https://www.schneier.com/blog/archives/2018/10/security_in_a_w.html), from [coffee machines](https://arstechnica.com/gadgets/2014/03/keurigs-next-generation-of-coffee-machines-will-have-drm-lockdown/) to smartphones, from [trains](https://arstechnica.com/tech-policy/2023/12/manufacturer-deliberately-bricked-trains-repaired-by-competitors-hackers-find/) to [drones](https://yewtu.be/watch?v=Nxem6yTRC4M), the role of software in keeping functioning hardware in use and out of the landfill grows more critical every day.

For consumers, the environmental harm may be out-of-sight and out-of-mind. Yet the environment is registering its effects, from the CO2 pumped into the atmosphere to the landfills that receive our discarded devices at their end of life, and the air, soil, and waters around them&mdash;not to mention the people and animals.

{{< container class="text-center" >}}

![A young man is pictured burning electrical wires to recover copper at Agbogbloshie, Ghana, as another metal scrap worker arrives with more wires to be burned. A 2018 article in the "International Journal of Cancer" reports a [correlation](https://onlinelibrary.wiley.com/doi/abs/10.1002/ijc.31902) between proximity to e-waste burn sites and childhood lymphoma. ([Image](https://en.wikipedia.org/wiki/File:Agbogbloshie,_Ghana_-_September_2019.jpg) by Muntaka Chasant, published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license.)](/blog/images/sec1_800px-Agbogbloshie_Ghana_September_2019.webp)

{{< /container >}}

It is particularly devastating when you consider the environmental and social harm caused by e-waste, especially when e-waste is generated earlier than necessary because of premature obsolescence. The production and transportation of a device accounts for 50&ndash;80+% of its carbon footprint over its lifecycle. A [German Environment Report](http://www.uba.de/uba-info-medien/4316.html) estimates you’d need to use a computer for over 30 years before efficiency gains in newly-produced devices justify their purchase.

Furthermore, the extraction of rare earth metals in production consumes copious amounts of energy and takes place under miserable social conditions, often in the Global South. For disposal, devices are typically returned to the Global South for end-of-life treatment, where they pollute the environment as toxic waste and cause enormous damage to workers' health or even death.

{{< container class="text-center" >}}

![Apple's carbon footprint. From Apple (2019), "Environmental Responsibility Report: 2019 Progress Report, covering fiscal year 2018". (Image from KDE published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license. Design by Anita Sengupta.)](/blog/images/apple-report.png)

{{< /container >}}

## Giving Consumers What They Want

Globally, interest in environmental harm and sustainable goods has been [rising steadily from 2015 to 2021](https://www.worldwildlife.org/publications/an-eco-wakening-measuring-awareness-engagement-and-action-for-nature). In Europe, a [2020 Eurobarometer poll](https://europa.eu/eurobarometer/surveys/detail/2228) found that 50% of consumers indicate that two reasons they purchase new devices are performance issues and non-functioning software, and [8 in 10 consumers](https://europa.eu/eurobarometer/api/deliverable/download/file?deliverableId=72255) believe manufacturers should be required to make it easier to repair digital devices.

Free Software already gives consumers what they want, but most don't know it yet. Transparency in code makes lightweight, highly performative software possible, even on much older devices, while user autonomy ensures the right to repair when applications stop functioning.

{{< container class="text-center" >}}

![KDE’s popular multi-platform PDF reader and universal document viewer Okular was awarded the Blue Angel ecolabel in 2022. (Image from KDE published under a [CC-BY-4.0](https://spdx.org/licenses/CC-BY-4.0.html) license.)](/blog/images/sec2_okular-BE-logo.png)

{{< /container >}}

In fact, the [Blue Angel criteria for desktop software](https://www.blauer-engel.de/en/productworld/resources-and-energy-efficient-software-products) are at the forefront in recognizing the critical role of transparency and user autonomy in sustainable software design. From 2021-2023, the KDE Eco project "Blauer Engel Für FOSS" (BE4FOSS) had the goal of collecting and spreading information about the Blue Angel ecolabel among developer communities. In 2022, KDE’s popular PDF and universal document reader [Okular](https://okular.kde.org/eco) became the [first ever Blue Angel eco-certified software](https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/)! The BE4FOSS project culminated with the KDE Eco handbook "Applying The Blue Angel Criteria To Free Software", which you can read [here](https://eco.kde.org/handbook/). KDE's [Sustainable Software](https://community.kde.org/Goals/Sustainable_Software) goal has continued this work by developing [emulation tools](https://eco.kde.org/categories/kdeecotest/) like [KdeEcoTest](https://eco.kde.org/blog/2024-02-20-windows-kdeecotest-support/) and [Selenium AT-SPI](https://eco.kde.org/categories/selenium/) to measure software's energy consumption in KDE's [KEcoLab](https://eco.kde.org/categories/kecolab/).

Now we want to take what we have achieved and bring it directly to eco-consumers.

Through educational campaigns and workshops, the "Opt Green" project aims to combat e-waste by keeping hardware in use with Free Software. Although the problem of software-driven e-waste is relevant for an increasing number of digital devices, the focus will be on desktop PCs, laptops, and, when possible, smartphones and tablets. We are planning to set up info-stands at fair-trade, organic, and artisanal markets as well as sustainability festivals such as the [Umweltfestival](https://www.umweltfestival.de/) in Berlin. We will distribute [leaflets](https://invent.kde.org/teams/eco/opt-green/-/blob/master/materials/leaflets/EN/kde-eco-umweltfestival-flyer-EN_final.jpg) to consumers, and demo vendor-abandoned devices which are not only usable, but also a joy to use thanks to the tireless work of inspiring Free Software communities. Installation workshops will give users the know-how to keep their devices in use for as long as they want.

Consumers don’t need a new computer to get secure, cutting-edge software; they just need the right software. Free Software already gives consumers what they want today, and we will be working hard to make sure they know that.

## Ready To Join Us?

Consumers want sustainable and repairable digital devices. We believe that providing users the software to keep devices in use and out of the landfill will drive demand for Free Software products and enable long-term hardware use.

**Do you want to join us in this movement to combat e-waste with Free Software? See our [contact info](https://eco.kde.org/get-involved/) to get involved.**

We need volunteers like you to bring the "Opt Green" campaign to towns and cities around the world. We need volunteers like you to design engaging guides and beautiful materials for global distribution. We need volunteers like you to report on the project in magazines and newspapers. Let's work together to bring sustainable software to your community!

Maybe you are interested in contributing to the development of [measurement tools](https://invent.kde.org/teams/eco/) like KdeEcoTest and Selenium AT-SPI or improving KEcoLab automation? Or using such tools to measure your software application's energy consumption? Let's collaborate to make energy transparency a part of Free Software development today!

Or maybe you actively contribute to a Free Software project that will keep hardware in use for longer. Please be in touch! We want to promote the amazing work you do directly with consumers.

Additional ideas are more than welcome. Part of the project will be figuring out what works and engagement by people like you will make this project a success. We would love to have you join us. Learn more: https://eco.kde.org/get-involved/

*Note: This post was updated on Monday, 17 June 2024.*

#### Funding Notice

This project is funded by the Federal Environment Agency and the Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>). The funds are made available by resolution of the German Bundestag.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="BMUV logo" width="340px"/>

<img src="/blog/images/uba.jpg" alt="UBA logo" width="250px"/>

{{< /container >}}

The publisher is responsible for the content of this publication.
