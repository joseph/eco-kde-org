---
author: KDE Eco
date: 2025-02-16
title: "Installation Workshop at Amerika-Gedenkbibliothek"
thumbnail: "/events/images/2025-02-16_event-amerika-gedenkbibliothek-bring-it-back-to-life.png"
location: "Berlin, DE"
featured: true
lang: "en"
---
