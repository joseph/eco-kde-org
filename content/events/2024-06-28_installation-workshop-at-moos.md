---
author: KDE Eco
date: 2024-06-28
title: "Installation Workshop at Moos"
thumbnail: "/events/images/2024-06-28_event-moos-bring-it-back-to-life.png"
location: "Berlin, DE"
featured: true
lang: "en"
---
